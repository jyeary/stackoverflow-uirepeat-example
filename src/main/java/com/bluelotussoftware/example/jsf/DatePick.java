/*
 * $Id$
 */
package com.bluelotussoftware.example.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@Named
@ConversationScoped
public class DatePick implements Serializable {

    private static final long serialVersionUID = -5061581851476260511L;
    @Inject
    private Conversation conversation;

    public DatePick() {
    }

    @PostConstruct
    private void init() {
        conversation.begin();
        System.out.println("Conversation " + conversation.getId() + " began.");
    }

    public List<String> getTimeSlot() {
        List<String> dates = new ArrayList<String>() {
            private static final long serialVersionUID = 3109256773218160485L;

            {
                add("01/1/2012");
                add("02/1/2012");
                add("03/1/2012");
                add("04/1/2012");
                add("05/1/2012");
                add("06/1/2012");
                add("07/1/2012");
                add("08/1/2012");
                add("09/1/2012");
                add("10/1/2012");
                add("11/1/2012");
                add("12/1/2012");
            }
        };
        return dates;
    }

    public void endConversation() {
        System.out.println("Conversation " + conversation.getId() + " ended.");
        conversation.end();
    }
}
